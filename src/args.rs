use crate::{
    error::{ParseError, RenderError},
    gfm::{GitHubMarkdownConfig, GitHubMarkdownParser},
    json::JsonParser,
    markdown::{MarkdownConfig, MarkdownParser},
    primitives::{BooleanParser, FloatParser, IntParser, StringParser},
    source::SourceParser,
    yaml::YamlParser,
};
use clap::Args;
use std::{
    fs::{self, OpenOptions},
    io::stdout,
    path::Path,
    str::FromStr,
};
use tera::Tera;

/// Describes a mapping for a variable and a source for its values. The source can be interpreted
/// as either a path or a raw value.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct VarMapping {
    pub variable: String,
    pub source: String,
}

impl VarMapping {
    pub fn new(variable: &str, source: &str) -> VarMapping {
        VarMapping {
            variable: variable.to_string(),
            source: source.to_string(),
        }
    }
}

impl FromStr for VarMapping {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut splits = s.splitn(2, ':');
        let variable = splits.next().ok_or(ParseError::InvalidVarMappingFormat)?;
        let source = splits.next().ok_or(ParseError::InvalidVarMappingFormat)?;

        Ok(VarMapping::new(variable, source))
    }
}

#[derive(Args, Debug)]
pub struct SupportedMappings {
    /// Support for sourcing JSON files.
    #[arg(
        long,
        help = "Map a variable to the contents of a JSON file. Format: <variable:path>."
    )]
    json: Vec<VarMapping>,

    /// Support for sourcing YAML files.
    #[arg(
        long,
        help = "Map a variable to the contents of a YAML file. Format: <variable:path>."
    )]
    yaml: Vec<VarMapping>,

    /// Support for sourcing Markdown files.
    #[arg(
        long,
        help = "Map a variable to the contents of a Markdown file. Format: <variable:path>."
    )]
    markdown: Vec<VarMapping>,

    /// Support for sourcing GitHub-flavored Markdown files.
    #[arg(
        long,
        help = "Map a variable to the contents of a GitHub-flavored Markdown file. Format: <variable:path>."
    )]
    gfm: Vec<VarMapping>,

    /// Support for primitives.
    #[arg(
        long,
        help = "Map a variable to a static string value. Format: <variable:str>."
    )]
    string: Vec<VarMapping>,

    #[arg(
        long,
        help = "Map a variable to a static int value. Format: <variable:int>."
    )]
    int: Vec<VarMapping>,

    #[arg(
        long,
        help = "Map a variable to a static float value. Format: <variable:float>."
    )]
    float: Vec<VarMapping>,

    #[arg(
        long,
        help = "Map a variable to a bool float value. Format: <variable:bool>."
    )]
    bool: Vec<VarMapping>,

    /// Markdown config for both parsers.
    #[command(flatten)]
    markdown_config: MarkdownConfig,

    #[command(flatten)]
    gfm_config: GitHubMarkdownConfig,
}

impl SupportedMappings {
    /// Populate the Tera context with all supported mappings.
    pub fn populate_tera_context(
        &self,
        tera_context: &mut tera::Context,
    ) -> Result<(), RenderError> {
        JsonParser {}.parse_many(tera_context, self.json.iter())?;
        YamlParser {}.parse_many(tera_context, self.yaml.iter())?;
        self.markdown_parser()
            .parse_many(tera_context, self.markdown.iter())?;
        self.gfm_parser()
            .parse_many(tera_context, self.gfm.iter())?;
        StringParser {}.parse_many(tera_context, self.string.iter())?;
        IntParser {}.parse_many(tera_context, self.int.iter())?;
        FloatParser {}.parse_many(tera_context, self.float.iter())?;
        BooleanParser {}.parse_many(tera_context, self.bool.iter())?;

        Ok(())
    }

    pub fn markdown_parser(&self) -> MarkdownParser {
        MarkdownParser {
            config: self.markdown_config,
        }
    }

    pub fn gfm_parser(&self) -> GitHubMarkdownParser {
        GitHubMarkdownParser {
            config: self.gfm_config,
        }
    }
}

#[derive(Args, Debug)]
pub struct OutputSettings {
    #[arg(
        long,
        short,
        help = "Write directly to the specified file. The file name is a Tera template."
    )]
    output: Option<String>,

    #[arg(
        long,
        short = 'f',
        help = "Overwrite an existing file",
        default_value = "false"
    )]
    overwrite: bool,
}

impl OutputSettings {
    pub fn render(
        &self,
        template: &str,
        tera: &mut Tera,
        context: &mut tera::Context,
    ) -> Result<(), RenderError> {
        let result = if let Some(output) = &self.output {
            // Render the output file name if provided. This allows for things like slugified names or dates.
            let rendered_name = tera
                .render_str(output, context)
                .map_err(RenderError::TeraRenderError)?;
            let mut fs = OpenOptions::new();
            let fpath = Path::new(&rendered_name);

            if self.overwrite && Path::new(&fpath).exists() {
                fs::remove_file(fpath)?;
            }

            let file = fs.create_new(true).write(true).open(fpath)?;

            tera.render_to(template, context, file)
        } else {
            tera.render_to(template, context, stdout())
        };

        result.map_err(RenderError::TeraRenderError)
    }
}
