use crate::{
    error::RenderError,
    source::{RawSource, Source, SourceParser},
};
use std::str::FromStr;

pub struct StringParser;

impl SourceParser for StringParser {
    type Source = RawSource;
    type Output = String;

    fn parse_content<'a>(
        &self,
        content: <Self::Source as Source>::Output<'a>,
    ) -> Result<Self::Output, RenderError> {
        Ok(content.to_string())
    }
}

pub struct IntParser;

impl SourceParser for IntParser {
    type Source = RawSource;
    type Output = isize;

    fn parse_content<'a>(
        &self,
        content: <Self::Source as Source>::Output<'a>,
    ) -> Result<Self::Output, RenderError> {
        isize::from_str(content).map_err(RenderError::ParseIntError)
    }
}

pub struct FloatParser;

impl SourceParser for FloatParser {
    type Source = RawSource;
    type Output = f64;

    fn parse_content<'a>(
        &self,
        content: <Self::Source as Source>::Output<'a>,
    ) -> Result<Self::Output, RenderError> {
        f64::from_str(content).map_err(RenderError::ParseFloatError)
    }
}

pub struct BooleanParser;

impl SourceParser for BooleanParser {
    type Source = RawSource;
    type Output = bool;

    fn parse_content<'a>(
        &self,
        content: <Self::Source as Source>::Output<'a>,
    ) -> Result<Self::Output, RenderError> {
        bool::from_str(content).map_err(RenderError::ParseBoolError)
    }
}
