use std::{
    num::{ParseFloatError, ParseIntError},
    str::ParseBoolError,
};

use thiserror::Error;

#[derive(Debug, Error)]
pub enum ParseError {
    #[error("Invalid variable mapping format. Expected in the format <variable:mapping>.")]
    InvalidVarMappingFormat,
}

#[derive(Debug, Error)]
pub enum RenderError {
    #[error("Failed to read file: {0}.")]
    IoError(#[from] std::io::Error),

    #[error("Failed to parse json: {0}.")]
    ParseJsonError(serde_json::Error),

    #[error("Failed to parse markdown yaml metadata: {0}.")]
    ParseMarkdownMetadataError(serde_yaml::Error),

    #[error("Failed to parse yaml: {0}.")]
    ParseYamlError(serde_yaml::Error),

    #[error("Failed to parse raw int value: {0}.")]
    ParseIntError(ParseIntError),

    #[error("Failed to parse raw float value: {0}.")]
    ParseFloatError(ParseFloatError),

    #[error("Failed to parse raw bool value: {0}.")]
    ParseBoolError(ParseBoolError),

    #[error("Failed to initialize Tera: {0}.")]
    TeraInitError(tera::Error),

    #[error("Failed to render Tera template: {0}.")]
    TeraRenderError(tera::Error),
}
