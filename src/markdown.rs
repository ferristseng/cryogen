use crate::{
    error::RenderError,
    source::{FileSource, Source, SourceParser},
};
use clap::Args;
use pulldown_cmark::{html, Options, Parser};
use serde::Serialize;
use std::io::Read;

/// The Markdown metadata holder is a YAML object.
pub type MarkdownMetadata = serde_yaml::Value;

/// Value written to Tera context.
#[derive(Serialize)]
pub struct RenderedMarkdown {
    metadata: Option<MarkdownMetadata>,
    html: String,
}

impl RenderedMarkdown {
    /// Creates a new container for rendered markdown.
    pub const fn new(metadata: Option<MarkdownMetadata>, html: String) -> RenderedMarkdown {
        RenderedMarkdown { metadata, html }
    }
}

const DIVIDER: &[u8] = &[b'-', b'-', b'-'];
const NEWLINE: &[u8] = &[b'\n'];
const NEWLINE_ALT: &[u8] = &[b'\r', b'\n'];

/// Tries to read a YAML block from the beginning of an input buffer.
pub fn read_header(mut buf: &[u8]) -> Result<(Option<MarkdownMetadata>, usize), RenderError> {
    let mut total_read = 0;
    // Check to see if the buffer starts with the YAML block divider.
    if !buf.starts_with(DIVIDER) {
        return Ok((None, 0));
    }

    total_read += DIVIDER.len();

    buf = &buf[DIVIDER.len()..];

    // Check to see if the DIVIDER is followed by a new line.
    if buf.starts_with(NEWLINE_ALT) {
        total_read += NEWLINE_ALT.len();

        buf = &buf[NEWLINE_ALT.len()..];
    } else if buf.starts_with(NEWLINE) {
        total_read += NEWLINE.len();

        buf = &buf[NEWLINE.len()..];
    } else {
        return Ok((None, 0));
    }

    let mut yaml_end = None;
    for (index, part) in buf.windows(DIVIDER.len()).enumerate() {
        if part == DIVIDER {
            let after_index = index + DIVIDER.len();

            // The YAML block could end at the EOF.
            //
            // Usually the YAML block will end before the
            // end of the file, and a newline will signify
            // the start of the Markdown code.
            //
            if after_index > buf.len() {
                total_read += after_index;

                yaml_end = Some(index);

                break;
            }

            if buf[after_index..].starts_with(NEWLINE) {
                total_read += after_index + NEWLINE.len();

                yaml_end = Some(index);

                break;
            }
            if buf[after_index..].starts_with(NEWLINE_ALT) {
                total_read += after_index + NEWLINE_ALT.len();

                yaml_end = Some(index);

                break;
            }
        }
    }

    match yaml_end {
        Some(0) => Ok((None, total_read)),
        Some(end) => serde_yaml::from_slice(&buf[..end])
            .map(|meta| (Some(meta), total_read))
            .map_err(RenderError::ParseMarkdownMetadataError),
        None => Ok((None, 0)),
    }
}

/// CLI argumetns for configuring the Markdown parser.
#[derive(Args, Clone, Copy, Debug)]
pub struct MarkdownConfig {
    #[arg(long = "markdown-yaml-metadata", help = "Enable YAML metadata block")]
    yaml_metadata: bool,

    #[arg(long = "markdown-footnotes", help = "Enable footnotes")]
    footnotes: bool,

    #[arg(long = "markdown-tables", help = "Enable tables")]
    tables: bool,
}

pub struct MarkdownParser {
    pub config: MarkdownConfig,
}

impl SourceParser for MarkdownParser {
    type Source = FileSource;
    type Output = RenderedMarkdown;

    fn parse_content<'a>(
        &self,
        mut content: <Self::Source as Source>::Output<'a>,
    ) -> Result<Self::Output, RenderError> {
        let mut opts = Options::empty();

        if self.config.footnotes {
            opts.insert(pulldown_cmark::Options::ENABLE_FOOTNOTES);
        }

        if self.config.tables {
            opts.insert(pulldown_cmark::Options::ENABLE_TABLES);
        }

        let mut buf = String::new();
        content.read_to_string(&mut buf)?;

        // Determine where the YAML metadata ends, and adjust the internal view of the buffer
        // accordingly.
        let mut view = &buf[..];
        let metadata = if self.config.yaml_metadata {
            let (metadata, md_start) = read_header(view.as_bytes())?;
            view = &buf[md_start..];
            metadata
        } else {
            None
        };

        let mut html = String::new();
        let parser = Parser::new_ext(view, opts);

        html::push_html(&mut html, parser);

        Ok(RenderedMarkdown::new(metadata, html.to_string()))
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_empty_prelude_recognized() {
        let yaml_block = b"---\n---\n";

        assert!(super::read_header(yaml_block).is_ok())
    }

    #[test]
    fn test_empty_string() {
        assert!(super::read_header(b"").is_ok());
    }

    #[test]
    fn test_incomplete_block() {
        assert!(super::read_header(b"---").is_ok());
    }

    #[test]
    fn test_valid_block() {
        assert!(super::read_header(b"---\ntitle: ABC\n---\n")
            .map(|(res, _)| res.is_some())
            .unwrap_or(false));
    }
}
