use crate::{args::VarMapping, error::RenderError};
use serde::Serialize;
use serde_json::Value;
use std::fs::{File, OpenOptions};
use tracing::{debug, debug_span, warn};

pub trait SourceParser {
    type Source: Source;
    type Output: Serialize;

    /// Parses a single mapping and puts it into the Tera context.
    fn parse(
        &self,
        tera_context: &mut tera::Context,
        mapping: &VarMapping,
    ) -> Result<(), RenderError> {
        let content = Self::Source::read_mapped_source(mapping)?;
        let parsed = self.parse_content(content)?;
        let exists = tera_context.contains_key(&mapping.variable);

        debug!("Binding key={}...", mapping.variable);

        if exists {
            warn!("Overwriting key={}!", mapping.variable);
        }

        tera_context.insert(&mapping.variable, &parsed);

        Ok(())
    }

    /// Parses multiple mappings and modifies the Tera context.
    fn parse_many<'a>(
        &self,
        tera_context: &mut tera::Context,
        mappings: impl Iterator<Item = &'a VarMapping>,
    ) -> Result<(), RenderError> {
        let _span = debug_span!("parse_many").entered();
        for mapping in mappings {
            self.parse(tera_context, mapping)?;
        }

        Ok(())
    }

    fn parse_indexed<S>(&self, sources: impl Iterator<Item = S>) -> Result<Vec<Value>, RenderError>
    where
        S: AsRef<str>,
    {
        let _span = debug_span!("parse_indexed").entered();
        let mut indexed = vec![];
        for source in sources {
            let content = Self::Source::read(&source)?;
            let parsed = self.parse_content(content)?;

            // Internally represent indexed data as JSON. Eventualy, it will be rendered as a
            // single vector and passed to the index template.
            indexed.push(
                serde_json::to_value(parsed)
                    .expect("expected content to be internally represented as JSON"),
            );
        }

        Ok(indexed)
    }

    fn parse_content<'a>(
        &self,
        content: <Self::Source as Source>::Output<'a>,
    ) -> Result<Self::Output, RenderError>;
}

pub trait Source {
    type Output<'a>;

    fn read_mapped_source<'a>(mapping: &'a VarMapping) -> Result<Self::Output<'a>, RenderError> {
        Self::read(&mapping.source)
    }

    fn read<'a, S>(source: &'a S) -> Result<Self::Output<'a>, RenderError>
    where
        S: AsRef<str>;
}

/// Treats the file specified by the mapped value as the content.
pub struct FileSource;

impl Source for FileSource {
    type Output<'a> = File;

    fn read<'a, S>(source: &'a S) -> Result<Self::Output<'a>, RenderError>
    where
        S: AsRef<str>,
    {
        let mut fs = OpenOptions::new();

        fs.read(true)
            .open(source.as_ref())
            .map_err(RenderError::IoError)
    }
}

/// Treats the mapped value as the content.
pub struct RawSource;

impl Source for RawSource {
    type Output<'a> = &'a str;

    fn read<'a, S>(source: &'a S) -> Result<Self::Output<'a>, RenderError>
    where
        S: AsRef<str>,
    {
        Ok(source.as_ref())
    }
}
