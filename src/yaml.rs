use crate::{
    error::RenderError,
    source::{FileSource, Source, SourceParser},
};

pub struct YamlParser;

impl SourceParser for YamlParser {
    type Source = FileSource;
    type Output = serde_yaml::Value;

    fn parse_content<'a>(
        &self,
        content: <Self::Source as Source>::Output<'a>,
    ) -> Result<Self::Output, RenderError> {
        serde_yaml::from_reader(content).map_err(RenderError::ParseYamlError)
    }
}
