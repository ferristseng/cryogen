use args::{OutputSettings, SupportedMappings};
use clap::{Args, Parser, Subcommand};
use error::RenderError;
use json::JsonParser;
use source::SourceParser;
use std::io::stdout;
use tera::Tera;
use tracing::Level;
use yaml::YamlParser;

mod args;
mod error;
mod gfm;
mod json;
mod markdown;
mod primitives;
mod source;
mod yaml;

#[derive(Parser, Debug)]
#[command(author, version)]
#[command(about = "Tools for rendering templates from data files")]
struct Cryogen {
    #[arg(
        long,
        help = "Path to Tera templates to load (e.g. for base templates or macros",
        global = true
    )]
    load_templates: Option<String>,

    #[arg(
        long,
        short,
        help = "Verbose logging",
        global = true,
        default_value = "false"
    )]
    verbose: bool,

    #[command(subcommand)]
    command: RenderCommands,
}

impl Cryogen {
    fn get_renderer(&self) -> Result<Tera, RenderError> {
        if let Some(ref templates) = self.load_templates {
            Tera::new(templates).map_err(RenderError::TeraInitError)
        } else {
            Ok(Tera::default())
        }
    }

    fn run(self) -> Result<(), RenderError> {
        if self.verbose {
            tracing_subscriber::fmt()
                .compact()
                .with_max_level(Level::DEBUG)
                .init();
        }

        let mut tera = self.get_renderer()?;
        let mut context = tera::Context::new();

        match self.command {
            RenderCommands::RenderSingle { command } => command.render(&mut tera, &mut context),
            RenderCommands::RenderIndex { command } => command.render(&mut tera, &mut context),
        }
    }
}

#[derive(Args, Debug)]
struct RenderSingleCommand {
    /// The template to render.
    #[arg()]
    template: String,

    #[command(flatten)]
    output: OutputSettings,

    /// Supported mappings.
    #[command(flatten)]
    mappings: SupportedMappings,
}

impl RenderSingleCommand {
    fn render(&self, tera: &mut Tera, context: &mut tera::Context) -> Result<(), RenderError> {
        tera.add_template_file(&self.template, None)
            .map_err(RenderError::TeraInitError)?;
        self.mappings.populate_tera_context(context)?;

        self.output.render(&self.template, tera, context)
    }
}

#[derive(Args, Debug)]
struct RenderIndexCommand {
    /// The template to render.
    #[arg()]
    template: String,

    /// Supported mappings within a global context.
    #[command(flatten)]
    global_mappings: SupportedMappings,

    #[command(flatten)]
    output: OutputSettings,

    #[arg(
        long,
        help = "Paginate the index with the specified number of results per-page"
    )]
    paginate: Option<usize>,

    #[arg(long, help = "Index a list of JSON files", num_args = 0..)]
    index_json: Vec<String>,

    #[arg(long, help = "Index a list of YAML files", num_args = 0..)]
    index_yaml: Vec<String>,

    #[arg(long, help = "Index a list of Markdown files", num_args = 0..)]
    index_markdown: Vec<String>,

    #[arg(long, help = "Index a list of GitHub-flavored Markdown files", num_args = 0..)]
    index_gfm: Vec<String>,
}

impl RenderIndexCommand {
    fn render(&self, tera: &mut Tera, context: &mut tera::Context) -> Result<(), RenderError> {
        tera.add_template_file(&self.template, None)
            .map_err(RenderError::TeraInitError)?;
        self.global_mappings.populate_tera_context(context)?;

        let mut indexed: Vec<serde_json::Value> = vec![];
        indexed.extend(JsonParser {}.parse_indexed(self.index_json.iter())?);
        indexed.extend(YamlParser {}.parse_indexed(self.index_yaml.iter())?);
        indexed.extend(
            self.global_mappings
                .markdown_parser()
                .parse_indexed(self.index_markdown.iter())?,
        );
        indexed.extend(
            self.global_mappings
                .gfm_parser()
                .parse_indexed(self.index_gfm.iter())?,
        );

        context.insert("index", &indexed);

        self.output.render(&self.template, tera, context)
    }
}

#[derive(Subcommand, Debug)]
enum RenderCommands {
    /// Renders a single Tera template.
    #[command(name = "single")]
    RenderSingle {
        #[command(flatten)]
        command: RenderSingleCommand,
    },

    /// Renders a single Tera template given an index of data files.
    #[command(name = "index")]
    RenderIndex {
        #[command(flatten)]
        command: RenderIndexCommand,
    },
}

fn main() -> Result<(), RenderError> {
    Cryogen::parse().run()
}
