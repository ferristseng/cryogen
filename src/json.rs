use crate::{
    error::RenderError,
    source::{FileSource, Source, SourceParser},
};

pub struct JsonParser;

impl SourceParser for JsonParser {
    type Source = FileSource;
    type Output = serde_json::Value;

    fn parse_content<'a>(
        &self,
        content: <Self::Source as Source>::Output<'a>,
    ) -> Result<Self::Output, RenderError> {
        serde_json::from_reader(content).map_err(RenderError::ParseJsonError)
    }
}
