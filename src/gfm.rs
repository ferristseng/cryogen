use crate::{
    error::RenderError,
    markdown::{read_header, RenderedMarkdown},
    source::{FileSource, Source, SourceParser},
};
use clap::Args;
use comrak::{
    markdown_to_html, ComrakExtensionOptions, ComrakOptions, ComrakParseOptions,
    ComrakRenderOptions, ListStyleType,
};
use std::io::Read;

#[derive(Args, Clone, Copy, Debug)]
pub struct GitHubMarkdownConfig {
    #[arg(long = "gfm-yaml-metadata", help = "Enable YAML metadata block")]
    gfm_yaml_metadata: bool,

    #[arg(
        long = "gfm-hardbreaks",
        help = "Convert soft breaks to hard breaks in output"
    )]
    hardbreaks: bool,

    #[arg(
        long = "gfm-smart-punctuation",
        help = "Convert punctuation to unicode equivalents"
    )]
    smart_punctuation: bool,

    #[arg(long = "gfm-github-pre-lang", help = "Enable GitHub pre blocks")]
    github_pre_lang: bool,

    #[arg(
        long = "gfm-safe",
        help = "Disable rendering raw HTML and dangerous links"
    )]
    safe: bool,

    #[arg(long = "gfm-strikethrough", help = "Enable strikethrough syntax")]
    strikethrough: bool,

    #[arg(long = "gfm-tag-filter", help = "Disable some raw HTML tags")]
    tag_filter: bool,

    #[arg(long = "gfm-tables", help = "Enable tables")]
    gfm_tables: bool,

    #[arg(long = "gfm-autolink", help = "Enable autolinks")]
    autolink: bool,

    #[arg(long = "gfm-tasklists", help = "Enable task lists")]
    tasklists: bool,

    #[arg(long = "gfm-superscript", help = "Enable superscript syntax")]
    superscript: bool,

    #[arg(long = "gfm-footnotes", help = "Enable footnotes")]
    gfm_footnotes: bool,

    #[arg(long = "gfm-description-lists", help = "Enable description-lists")]
    description_lists: bool,

    // #[arg(long = "gfm-list-style-type", help = "List style type in output")]
    // list_style_type: ListStyleType,
    #[arg(
        long = "gfm-sourcepos",
        help = "Include source position attribute in output"
    )]
    sourcepos: bool,
}

pub struct GitHubMarkdownParser {
    pub config: GitHubMarkdownConfig,
}

impl SourceParser for GitHubMarkdownParser {
    type Source = FileSource;
    type Output = RenderedMarkdown;

    fn parse_content<'a>(
        &self,
        mut content: <Self::Source as Source>::Output<'a>,
    ) -> Result<Self::Output, RenderError> {
        let opts = ComrakOptions {
            render: ComrakRenderOptions {
                hardbreaks: self.config.hardbreaks,
                github_pre_lang: self.config.github_pre_lang,
                full_info_string: true,
                unsafe_: !self.config.safe,
                escape: true,
                width: 80,
                list_style: ListStyleType::Star,
                sourcepos: self.config.sourcepos,
            },
            parse: ComrakParseOptions {
                smart: self.config.smart_punctuation,
                default_info_string: None,
                relaxed_tasklist_matching: true,
            },
            extension: ComrakExtensionOptions {
                strikethrough: self.config.strikethrough,
                tagfilter: self.config.tag_filter,
                table: self.config.gfm_tables,
                autolink: self.config.autolink,
                tasklist: self.config.tasklists,
                superscript: self.config.superscript,
                footnotes: self.config.gfm_footnotes,
                description_lists: self.config.description_lists,
                front_matter_delimiter: None,
                header_ids: None,
            },
        };

        let mut buf = String::new();
        content.read_to_string(&mut buf)?;

        let mut view = &buf[..];
        let metadata = if self.config.gfm_yaml_metadata {
            let (metadata, md_start) = read_header(view.as_bytes())?;
            view = &view[md_start..];
            metadata
        } else {
            None
        };

        let html = markdown_to_html(view, &opts);

        Ok(RenderedMarkdown::new(metadata, html))
    }
}
